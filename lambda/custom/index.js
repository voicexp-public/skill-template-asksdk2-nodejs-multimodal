/* eslint-disable  func-names */
/* eslint-disable  no-console */
/*jshint esversion: 6 */

const Alexa = require('ask-sdk');
const isLambda = require('is-lambda');

if (!isLambda) {
    // allow local debugging
    const AWS = require('aws-sdk');
    AWS.config.update({region:'us-east-1'});
}

const skillBuilder = Alexa.SkillBuilders.standard();


exports.handler = skillBuilder
  .addRequestHandlers(
    ...require('./handlers/coreHandlers'),
    ...require('./handlers/sampleHandlers'),
    require('./handlers/notImplementedHandler') //always the last one, catchall for intents in model but not implemented
  )
  .addErrorHandlers(...require('./handlers/errorHandlers'))
  .addRequestInterceptors(
    require('./interceptors/logRequestInterceptor'),
    require('./interceptors/conversationModeRequestInterceptor'),
    require('./interceptors/supportsDisplayRequestInterceptor'),
    require('./interceptors/localizationRequestInterceptor')
  )
  .addResponseInterceptors(
    require('./interceptors/logResponseInterceptor'),
    require('./interceptors/repeatRememberResponseInterceptor')
  )
  // .withTableName('skillname-env-users')
  // .withAutoCreateTable(true)
  .lambda();