/*jslint node: true */
/*jshint esversion: 6 */

'use strict';

const Alexa = require('ask-sdk');


// let image = new Alexa.ImageHelper().addImageInstance('url').getImage();

// let richTextContent = new Alexa.RichTextContentHelper()
//     .withPrimaryText('')
//     .withSecondaryText('')
//     .withTertiaryText('')
//     .getTextContent();

// let plainTextContent = new Alexa.PlainTextContentHelper()
//     .withPrimaryText('')
//     .withSecondaryText('')
//     .withTertiaryText('')
//     .getTextContent();


function renderBodyTemplate1Screen(params) {

    const backgroundImage = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/1000/600/nature/grayscale?t=1526483146956').getImage();

    const textContent = new Alexa.RichTextContentHelper()
        .withPrimaryText(`<div align='center'><font size='7'><b>Kindness begins with me.</b></font> <font size='5'><br/><br/>Clara W. McMaster</font></div>`)
        .getTextContent();

    const template = {
        type: 'BodyTemplate1',
        backgroundImage: backgroundImage,
        textContent: textContent,
        backButton: 'HIDDEN'
    };

    return template;
}

function renderBodyTemplate2Screen(params) {

    const backgroundImage = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/1000/600/nature/grayscale?t=1526483146956').getImage();
    const image = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/320/320/animals?t=1526495424669').getImage();

    const textContent = new Alexa.RichTextContentHelper()
        .withPrimaryText(``)
        .getTextContent();

    const template = {
        type: 'BodyTemplate2',
        backgroundImage: backgroundImage,
        image: image,
        textContent: textContent,
        backButton: 'HIDDEN'
    };

    return template;
}

function renderBodyTemplate3Screen(params) {

    const backgroundImage = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/1000/600/nature/grayscale?t=1526483146956').getImage();
    const image = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/320/320/animals?t=1526495424669').getImage();

    const textContent = new Alexa.RichTextContentHelper()
        .withPrimaryText(``)
        .getTextContent();

    const template = {
        type: 'BodyTemplate3',
        backgroundImage: backgroundImage,
        image: image,
        textContent: textContent,
        backButton: 'HIDDEN'
    };

    return template;
}

function renderBodyTemplate6Screen(params) {

    const backgroundImage = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/1000/600/nature/grayscale?t=1526483146956').getImage();

    const textContent = new Alexa.RichTextContentHelper()
        .withPrimaryText(``)
        .getTextContent();

    const template = {
        type: 'BodyTemplate6',
        backgroundImage: backgroundImage,
        textContent: textContent,
        backButton: 'HIDDEN'
    };

    return template;
}

function renderBodyTemplate7Screen(params) {

    const backgroundImage = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/1000/600/nature/grayscale?t=1526494431405').getImage();
    const image = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/924/500/animals?t=1526494860009').getImage();


    const template = {
        type: 'BodyTemplate7',
        backgroundImage: backgroundImage,
        image: image,
        backButton: 'HIDDEN'
    };

    return template;
}

function renderListTemplate1Screen(params) {

    const title = 'List Template 1';
    const backgroundImage = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/1000/600/nature/grayscale?t=1526494431405').getImage();
    const image = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/924/500/animals?t=1526494860009').getImage();

    const itemList = [
        {
            token: 'item-1',
            textContent: new Alexa.RichTextContentHelper()
                .withPrimaryText(`Item 1 Title`)
                .withSecondaryText(`This is a longer description`)
                .withTertiaryText(`2/12/18`)
                .getTextContent()
        }
    ];


    const template = {
        type: 'ListTemplate1',
        token: 'screen-1',
        title: title,
        // backgroundImage: backgroundImage,
        backButton: 'HIDDEN',
        listItems: itemList
    };

    return template;
}

function renderListTemplate2Screen(params) {

    const title = 'List Template 1';
    const backgroundImage = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/1000/600/nature/grayscale?t=1526494431405').getImage();
    const image = new Alexa.ImageHelper().addImageInstance('https://placeimg.com/924/500/animals?t=1526494860009').getImage();

    const itemList = [
        {
            token: 'item-1',
            textContent: new Alexa.RichTextContentHelper()
                .withPrimaryText(`Item 1 Title`)
                .withSecondaryText(`This is a longer description`)
                .withTertiaryText(`2/12/18`)
                .getTextContent()
        }
    ];


    const template = {
        type: 'ListTemplate2',
        token: 'screen-1',
        title: title,
        // backgroundImage: backgroundImage,
        backButton: 'HIDDEN',
        listItems: itemList
    };

    return template;
}

module.exports.renderBodyTemplate1Screen = renderBodyTemplate1Screen;
module.exports.renderBodyTemplate2Screen = renderBodyTemplate2Screen;
module.exports.renderBodyTemplate3Screen = renderBodyTemplate3Screen;
module.exports.renderBodyTemplate6Screen = renderBodyTemplate6Screen;
module.exports.renderBodyTemplate7Screen = renderBodyTemplate7Screen;
module.exports.renderListTemplate1Screen = renderListTemplate1Screen;
module.exports.renderListTemplate2Screen = renderListTemplate2Screen;