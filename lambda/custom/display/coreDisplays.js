/*jslint node: true */
/*jshint esversion: 6 */

'use strict';

const Alexa = require('ask-sdk');


// let image = new Alexa.ImageHelper().addImageInstance('url').getImage();

// let richTextContent = new Alexa.RichTextContentHelper()
//     .withPrimaryText('')
//     .withSecondaryText('')
//     .withTertiaryText('')
//     .getTextContent();

// let plainTextContent = new Alexa.PlainTextContentHelper()
//     .withPrimaryText('')
//     .withSecondaryText('')
//     .withTertiaryText('')
//     .getTextContent();


function renderWelcomeScreen(params) {

    const backgroundImage = new Alexa.ImageHelper().addImageInstance('https://s3.amazonaws.com/voicexp-skill-template-nodejs-multimodal/images/background-welcome.png').getImage();

    const template = {
        type: 'BodyTemplate6',
        backgroundImage: backgroundImage,
        backButton: 'HIDDEN'
    };

    return template;
}

function renderGoodbyeScreen(params) {

    const backgroundImage = new Alexa.ImageHelper().addImageInstance('https://s3.amazonaws.com/voicexp-skill-template-nodejs-multimodal/images/background-goodbye.png').getImage();

    const template = {
        type: 'BodyTemplate6',
        backgroundImage: backgroundImage,
        backButton: 'HIDDEN'
    };

    return template;
}


module.exports.renderWelcomeScreen = renderWelcomeScreen;
module.exports.renderGoodbyeScreen = renderGoodbyeScreen;