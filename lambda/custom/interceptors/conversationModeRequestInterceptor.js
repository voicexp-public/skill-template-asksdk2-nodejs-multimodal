/*jslint node: true */
/*jshint esversion: 6 */

const interceptor = {
    process(handlerInput) {

        if (handlerInput.requestEnvelope.session.new === true) {

            const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
            const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

            if (handlerInput.requestEnvelope.request.type === 'LaunchRequest') {
                sessionAttributes.isConversationMode = true;
            }
            else {
                sessionAttributes.isConversationMode = false;
            }

            requestAttributes.isFirstLaunch = true;
        }
    }
};

module.exports = interceptor;
