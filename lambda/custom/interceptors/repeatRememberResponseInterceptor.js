/*jslint node: true */
/*jshint esversion: 6 */

const interceptor = {
    process(handlerInput, response) {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
     
        if (response && response.shouldEndSession === false) {

            const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
            const translations = requestAttributes.getTranslations('RepeatDefault');
            let speech = translations.speechWithPrompt;
            let reprompt = translations.reprompt;


            //speech
            if (response.outputSpeech && response.outputSpeech.ssml) {
                speech = response.outputSpeech.ssml;
            }
            else if (response.outputSpeech && response.outputSpeech.text) {
                speech = response.outputSpeech.text;
            }


            //reprompt
            if (response.reprompt && response.reprompt.outputSpeech && response.reprompt.outputSpeech.ssml) {
                reprompt = response.reprompt.outputSpeech.ssml;
            }           
            else if (response.reprompt && response.reprompt.outputSpeech && response.reprompt.outputSpeech.text) {
                reprompt = response.reprompt.outputSpeech.text;
            }


            sessionAttributes.repeat = {
                speech: speech,
                reprompt: reprompt
            };
                        
        }
    }
};

module.exports = interceptor;
