/*jslint node: true */
/*jshint esversion: 6 */

const i18n = require('i18next');
const languageStrings = require('../translations.json');
const FALLBACK_LISTS_NAME = 'FallbackLists';
const sample = require('lodash.sample');

const interceptor = {
    process(handlerInput) {
        const localizationClient = i18n.init({
            lng: handlerInput.requestEnvelope.request.locale,
            resources: languageStrings,
            returnObjects: true
        });

        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

        requestAttributes.getTranslations = function (key, namedParams, isSpeech = true) {

            if (isSpeech) {

                let response = {
                    speech: null,
                    prompt: null,
                    reprompt: null,
                    displayHint: null,
                    displayTitle: null,
                    progressiveResponse: "",
                    itemTemplate: ""
                };

                let mainTranslations = localizationClient.t(key, namedParams);
                let fallbackTranslations = localizationClient.t(FALLBACK_LISTS_NAME);

                // get speech
                if (mainTranslations.speech) {
                    response.speech = mainTranslations.speech;
                }
                else if (mainTranslations.speechList) {
                    response.speech = sample(mainTranslations.speechList);
                }
                else {
                    response.speech = sample(fallbackTranslations.speechList);
                }

                // get prompt
                if (mainTranslations.prompt) {
                    response.prompt = mainTranslations.prompt;
                }
                else if (mainTranslations.promptList) {
                    response.prompt = sample(mainTranslations.promptList);
                }
                else {
                    response.prompt = sample(fallbackTranslations.promptList);
                }

                // used for conversation mode
                response.speechWithPrompt = response.speech + '<break time="300ms"/>' + response.prompt;

                // get reprompt
                if (mainTranslations.reprompt) {
                    response.reprompt = mainTranslations.reprompt;
                }
                else if (mainTranslations.repromptList) {
                    response.reprompt = sample(mainTranslations.repromptList);
                }
                else {
                    response.reprompt = sample(fallbackTranslations.repromptList);
                }

                // get displayHint
                if (mainTranslations.displayHint) {
                    response.displayHint = mainTranslations.displayHint;
                }
                else if (mainTranslations.displayHintList) {
                    response.displayHint = sample(mainTranslations.displayHintList);
                }
                else {
                    response.displayHint = sample(fallbackTranslations.displayHintList);
                }

                // get displayTitle
                if (mainTranslations.displayTitle) {
                    response.displayTitle = mainTranslations.displayTitle;
                }
                else if (mainTranslations.displayTitleList) {
                    response.displayTitle = sample(mainTranslations.displayTitleList);
                }
                else {
                    response.displayTitle = sample(fallbackTranslations.displayTitleList);
                }

                //get progressiveResponse
                if (mainTranslations.progressiveResponse) {
                    response.progressiveResponse = mainTranslations.progressiveResponse;
                }

                // set attributes so repeat works
                sessionAttributes.repeat = {
                    speech: response.speech,
                    prompt: response.prompt,
                    reprompt: response.reprompt
                };
 
                return response;
            }
            else {
                return localizationClient.t(key, namedParams);

            }
        };
    }
};

module.exports = interceptor;