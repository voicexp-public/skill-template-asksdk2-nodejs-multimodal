/*jslint node: true */
/*jshint esversion: 6 */

const interceptor = {
    process(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        requestAttributes.supportsDisplay = function () {
            
            if (handlerInput.requestEnvelope &&
                handlerInput.requestEnvelope.context &&
                handlerInput.requestEnvelope.context.System &&
                handlerInput.requestEnvelope.context.System.device &&
                handlerInput.requestEnvelope.context.System.device.supportedInterfaces &&
                handlerInput.requestEnvelope.context.System.device.supportedInterfaces.Display) {
                return true;
            }
            else {
                return false;
            }
        };
    }
};

module.exports = interceptor;