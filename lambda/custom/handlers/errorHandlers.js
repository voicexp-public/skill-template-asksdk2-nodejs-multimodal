/*jslint node: true */
/*jshint esversion: 6 */

const handlers = [

    // CatchAllError
    {
      canHandle() {
        return true;
      },
      handle(handlerInput, error) {
        console.log(`Error handled: ${error.message}`);
        console.log(`Stack: ${error.stack}`);
  
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
  
        const translations = requestAttributes.getTranslations('CatchAllError');
  
        const responseBuilder = handlerInput.responseBuilder
            .speak(translations.speechWithPrompt)
            .reprompt(translations.reprompt);
  
        return responseBuilder.getResponse();
      }
    }
  
  ];
  
  module.exports = handlers;        