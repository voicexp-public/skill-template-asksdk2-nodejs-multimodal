/*jslint node: true */
/*jshint esversion: 6 */

const handler = 

    // Not implemented
    {
        canHandle(handlerInput) {
            return handlerInput.requestEnvelope.request.type === 'IntentRequest';
        },
        handle(handlerInput) {
            console.log('coreHandlers ---> NotImplemented');

            const requestAttributes = handlerInput.attributesManager.getRequestAttributes();

            const translations = requestAttributes.getTranslations('NotImplemented');

            const responseBuilder = handlerInput.responseBuilder
                .speak(translations.speechWithPrompt)
                .reprompt(translations.reprompt);

            return responseBuilder.getResponse();
        }
    };

module.exports = handler;