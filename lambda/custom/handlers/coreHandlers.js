/*jslint node: true */
/*jshint esversion: 6 */

const display = require('../display/coreDisplays');

const handlers = [

    // LaunchRequest
    {
        canHandle(handlerInput) {
            return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
        },
        handle(handlerInput) {
            console.log('coreHandlers ---> LaunchRequest');

            const requestAttributes = handlerInput.attributesManager.getRequestAttributes();

            let translationKey = "WelcomeBack";

            if (requestAttributes.isFirstLaunch === true) {
                translationKey = "WelcomeFirst";
            }

            const translations = requestAttributes.getTranslations(translationKey);

            const responseBuilder = handlerInput.responseBuilder
                .speak(translations.speechWithPrompt)
                .reprompt(translations.reprompt);

            // render display
            if (requestAttributes.supportsDisplay()) {
                const displayTemplate = display.renderWelcomeScreen();

                responseBuilder
                    .addRenderTemplateDirective(displayTemplate)
                    .addHintDirective(translations.displayHint);
            }

            return responseBuilder.getResponse();
        }
    },

    // AMAZON.HelpIntent
    {
        canHandle(handlerInput) {
            return handlerInput.requestEnvelope.request.type === 'IntentRequest'
                && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
        },
        handle(handlerInput) {
            console.log('coreHandlers ---> AMAZON.HelpIntent');

            const requestAttributes = handlerInput.attributesManager.getRequestAttributes();

            const translations = requestAttributes.getTranslations('AMAZONHelpIntent');

            const responseBuilder = handlerInput.responseBuilder
                .speak(translations.speechWithPrompt)
                .reprompt(translations.reprompt);

            return responseBuilder.getResponse();
        }
    },

    // AMAZON.RepeatIntent
    {
        canHandle(handlerInput) {

            const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

            return handlerInput.requestEnvelope.request.type === 'IntentRequest'
                && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.RepeatIntent'
                && sessionAttributes.repeat;
        },
        handle(handlerInput) {
            console.log('coreHandlers ---> AMAZON.RepeatIntent');

            const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

            return handlerInput.responseBuilder
                .speak(sessionAttributes.repeat.speech)
                .reprompt(sessionAttributes.repeat.reprompt)
                .getResponse();

        }
    },

    // CancelStopIntent
    {
        canHandle(handlerInput) {
            return handlerInput.requestEnvelope.request.type === 'IntentRequest'
                && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                    || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
        },
        handle(handlerInput) {
            console.log('coreHandlers ---> CancelStopIntent');

            const requestAttributes = handlerInput.attributesManager.getRequestAttributes();

            const translations = requestAttributes.getTranslations('CancelStop');

            const responseBuilder = handlerInput.responseBuilder
                .speak(translations.speech);

            // render display
            if (requestAttributes.supportsDisplay()) {
                const displayTemplate = display.renderGoodbyeScreen();

                responseBuilder
                    .addRenderTemplateDirective(displayTemplate)
                    .addHintDirective(translations.displayHint);
            }

            return responseBuilder.getResponse();
        }
    },

    // AMAZON.FallbackIntent
    {
        canHandle(handlerInput) {
            return handlerInput.requestEnvelope.request.type === 'IntentRequest'
                && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent';
        },
        handle(handlerInput) {
            console.log('coreHandlers ---> AMAZON.FallbackIntent');

            const requestAttributes = handlerInput.attributesManager.getRequestAttributes();

            const translations = requestAttributes.getTranslations('AMAZONFallbackIntent');

            const responseBuilder = handlerInput.responseBuilder
                .speak(translations.speechWithPrompt)
                .reprompt(translations.reprompt);

            return responseBuilder.getResponse();
        }
    },

    // SessionEndedRequest
    {
        canHandle(handlerInput) {
            return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
        },
        handle(handlerInput) {
            console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

            if (handlerInput.requestEnvelope.request.reason === 'ERROR') {
                console.log("\n" + JSON.stringify(handlerInput.requestEnvelope.request.error, null, 2));
            }

            return handlerInput.responseBuilder.getResponse();
        }
    }

];

module.exports = handlers;