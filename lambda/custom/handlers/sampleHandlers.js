/*jslint node: true */
/*jshint esversion: 6 */

const display = require('../display/sampleDisplays');

const handlers = [

  // SampleNoDisplayIntent
  {
    canHandle(handlerInput) {
      return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'SampleNoDisplayIntent';
    },
    handle(handlerInput) {
      console.log('sampleHandlers ---> SampleNoDisplayIntent');

      const requestAttributes = handlerInput.attributesManager.getRequestAttributes();

      const translations = requestAttributes.getTranslations('SampleNoDisplayIntent');

      const responseBuilder = handlerInput.responseBuilder;

      if (sessionAttributes.isConversationMode) {
        responseBuilder
          .speak(translations.speechWithPrompt)
          .reprompt(translations.reprompt);
      }
      else {
        responseBuilder
          .speak(translations.speech)
          .withShouldEndSession(true);
      }

      return responseBuilder.getResponse();
    }
  },

  // BodyTemplateOneIntent
  {
    canHandle(handlerInput) {
      return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'BodyTemplateOneIntent';
    },
    handle(handlerInput) {
      console.log('sampleHandlers ---> BodyTemplateOneIntent');

      const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
      const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

      const translations = requestAttributes.getTranslations('BodyTemplateOneIntent');

      const responseBuilder = handlerInput.responseBuilder;

      if (sessionAttributes.isConversationMode) {
        responseBuilder
          .speak(translations.speechWithPrompt)
          .reprompt(translations.reprompt);
      }
      else {
        responseBuilder
          .speak(translations.speech)
          .withShouldEndSession(true);
      }

      // render display
      if (requestAttributes.supportsDisplay()) {
        const displayTemplate = display.renderBodyTemplate1Screen();

        responseBuilder
          .addRenderTemplateDirective(displayTemplate);
      }

      return responseBuilder.getResponse();
    }
  },

  // BodyTemplateTwoIntent
  {
    canHandle(handlerInput) {
      return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'BodyTemplateTwoIntent';
    },
    handle(handlerInput) {
      console.log('sampleHandlers ---> BodyTemplateTwoIntent');

      const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
      const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

      const translations = requestAttributes.getTranslations('BodyTemplateTwoIntent');

      const responseBuilder = handlerInput.responseBuilder;

      if (sessionAttributes.isConversationMode) {
        responseBuilder
          .speak(translations.speechWithPrompt)
          .reprompt(translations.reprompt);
      }
      else {
        responseBuilder
          .speak(translations.speech)
          .withShouldEndSession(true);
      }

      // render display
      if (requestAttributes.supportsDisplay()) {
        const displayTemplate = display.renderBodyTemplate2Screen();

        responseBuilder
          .addRenderTemplateDirective(displayTemplate)
          .addHintDirective(translations.displayHint);
      }

      return responseBuilder.getResponse();
    }
  },

  // BodyTemplateThreeIntent
  {
    canHandle(handlerInput) {
      return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'BodyTemplateThreeIntent';
    },
    handle(handlerInput) {
      console.log('sampleHandlers ---> BodyTemplateThreeIntent');

      const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
      const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

      const translations = requestAttributes.getTranslations('BodyTemplateThreeIntent');

      const responseBuilder = handlerInput.responseBuilder;

      if (sessionAttributes.isConversationMode) {
        responseBuilder
          .speak(translations.speechWithPrompt)
          .reprompt(translations.reprompt);
      }
      else {
        responseBuilder
          .speak(translations.speech)
          .withShouldEndSession(true);
      }

      // render display
      if (requestAttributes.supportsDisplay()) {
        const displayTemplate = display.renderBodyTemplate3Screen();

        responseBuilder
          .addRenderTemplateDirective(displayTemplate);
      }

      return responseBuilder.getResponse();
    }
  },

  // BodyTemplateSixIntent
  {
    canHandle(handlerInput) {
      return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'BodyTemplateSixIntent';
    },
    handle(handlerInput) {
      console.log('sampleHandlers ---> BodyTemplateSixIntent');

      const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
      const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

      const translations = requestAttributes.getTranslations('BodyTemplateSixIntent');

      const responseBuilder = handlerInput.responseBuilder;

      if (sessionAttributes.isConversationMode) {
        responseBuilder
          .speak(translations.speechWithPrompt)
          .reprompt(translations.reprompt);
      }
      else {
        responseBuilder
          .speak(translations.speech)
          .withShouldEndSession(true);
      }

      // render display
      if (requestAttributes.supportsDisplay()) {
        const displayTemplate = display.renderBodyTemplate6Screen();

        responseBuilder
          .addRenderTemplateDirective(displayTemplate)
          .addHintDirective(translations.displayHint);
      }

      return responseBuilder.getResponse();
    }
  },

  // BodyTemplateSevenIntent
  {
    canHandle(handlerInput) {
      return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'BodyTemplateSevenIntent';
    },
    handle(handlerInput) {
      console.log('sampleHandlers ---> BodyTemplateSevenIntent');

      const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
      const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

      const translations = requestAttributes.getTranslations('BodyTemplateSevenIntent');

      const responseBuilder = handlerInput.responseBuilder;

      if (sessionAttributes.isConversationMode) {
        responseBuilder
          .speak(translations.speechWithPrompt)
          .reprompt(translations.reprompt);
      }
      else {
        responseBuilder
          .speak(translations.speech)
          .withShouldEndSession(true);
      }

      // render display
      if (requestAttributes.supportsDisplay()) {
        const displayTemplate = display.renderBodyTemplate7Screen();

        responseBuilder
          .addRenderTemplateDirective(displayTemplate);
      }

      return responseBuilder.getResponse();
    }
  },

  // ListTemplateOneIntent
  {
    canHandle(handlerInput) {
      return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'ListTemplateOneIntent';
    },
    handle(handlerInput) {
      console.log('sampleHandlers ---> ListTemplateOneIntent');

      const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
      const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

      const translations = requestAttributes.getTranslations('ListTemplateOneIntent');

      const responseBuilder = handlerInput.responseBuilder;

      if (sessionAttributes.isConversationMode) {
        responseBuilder
          .speak(translations.speechWithPrompt)
          .reprompt(translations.reprompt);
      }
      else {
        responseBuilder
          .speak(translations.speech)
          .withShouldEndSession(true);
      }

      // render display
      if (requestAttributes.supportsDisplay()) {
        const displayTemplate = display.renderListTemplate1Screen();

        responseBuilder
          .addRenderTemplateDirective(displayTemplate);
      }

      return responseBuilder.getResponse();
    }
  },

  // ListTemplateTwoIntent
  {
    canHandle(handlerInput) {
      return handlerInput.requestEnvelope.request.type === 'IntentRequest'
        && handlerInput.requestEnvelope.request.intent.name === 'ListTemplateTwoIntent';
    },
    handle(handlerInput) {
      console.log('sampleHandlers ---> ListTemplateTwoIntent');

      const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
      const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

      const translations = requestAttributes.getTranslations('ListTemplateTwoIntent');

      const responseBuilder = handlerInput.responseBuilder;

      if (sessionAttributes.isConversationMode) {
        responseBuilder
          .speak(translations.speechWithPrompt)
          .reprompt(translations.reprompt);
      }
      else {
        responseBuilder
          .speak(translations.speech)
          .withShouldEndSession(true);
      }

      // render display
      if (requestAttributes.supportsDisplay()) {
        const displayTemplate = display.renderListTemplate2Screen();

        responseBuilder
          .addRenderTemplateDirective(displayTemplate);
      }

      return responseBuilder.getResponse();
    }
  }
];

module.exports = handlers;        